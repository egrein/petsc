#!/usr/bin/python
if __name__ == '__main__':
  import sys
  import os
  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
	'PETSC_ARCH=arch-win32-debug',
    '--with-cc=win32fe cl',
    '--with-cxx=win32fe cl',
	'--with-fc=win32fe ifort',
	'--with-ar=win32fe lib',
	'CFLAGS=-EHsc -Z7 -MDd',
	'CXXFLAGS=-EHsc -Z7 -MDd',
	'FFLAGS=-MD',
	'--doCleanup=1',
	'--with-clanguage=Cxx',
	'--with-c++-support',
	'--ignore-cygwin-link',
	'--with-blas-lib=[/cygdrive/f/Shared/dist/12.0-win32/mkl-11/lib/mkl_blas95.lib,/cygdrive/f/Shared/dist/12.0-win32/mkl-11/lib/mkl_rt.lib,\
		/cygdrive/f/Shared/dist/12.0-win32/mkl-11/compiler/lib/libiomp5md.lib]',
	'--with-lapack-lib=[/cygdrive/f/Shared/dist/12.0-win32/mkl-11/lib/mkl_lapack95.lib]',
    '--with-debugging=1',
	'--with-mpi-include=/cygdrive/c/Program Files/Microsoft HPC Pack 2008 SDK/Include',
	'--with-mpi-lib=[/cygdrive/c/Program Files/Microsoft HPC Pack 2008 SDK/Lib/i386/msmpi.lib,/cygdrive/c/Program Files/Microsoft HPC Pack 2008 SDK/Lib/i386/msmpifec.lib]',
	'--with-hypre-include=/home/ederson/external_packages/hypre-2.10.1_32_debug/include',
	'--with-hypre-lib=/home/ederson/external_packages/hypre-2.10.1_32_debug/lib/HYPRE.lib',
	'--with-threadsafety',
	'--download-concurrencykit',
	'--with-log=0',
	'--with-shared-libraries',
	'--useThreads=0',	
	'--useThreads=0',
  ]
  configure.petsc_configure(configure_options)
