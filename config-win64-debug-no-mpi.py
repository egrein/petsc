#!/usr/bin/python
if __name__ == '__main__':
  import sys
  import os
  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
	'PETSC_ARCH=arch-win64-debug-no-mpi',
    '--with-cc=win32fe cl',
    '--with-cxx=win32fe cl',
	'--with-fc=win32fe ifort',
	'--with-ar=win32fe lib',
	'CFLAGS=-EHsc -Z7 -MDd',
	'CXXFLAGS=-EHsc -Z7 -MDd',
	'FFLAGS=-MDd',
	'--doCleanup=1',
	'--with-clanguage=Cxx',
	'--with-c++-support',
	'--ignore-cygwin-link',
	'--with-blas-lib=[/cygdrive/f/Shared/dist/12.0-win64/mkl-11/lib/mkl_blas95_lp64.lib,/cygdrive/f/Shared/dist/12.0-win64/mkl-11/lib/mkl_rt.lib,\
		/cygdrive/f/Shared/dist/12.0-win64/mkl-11/compiler/lib/libiomp5md.lib]',
	'--with-lapack-lib=[/cygdrive/f/Shared/dist/12.0-win64/mkl-11/lib/mkl_lapack95_lp64.lib]',
    '--with-debugging=1',
	'--with-mpi=0',
	'--with-threadsafety',
	'--download-concurrencykit',
	'--with-log=0',
	'--with-shared-libraries',
	'--useThreads=0',	
	'--useThreads=0',
  ]
  configure.petsc_configure(configure_options)
