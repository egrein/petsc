#!/usr/bin/python
if __name__ == '__main__':
  import sys
  import os
  sys.path.insert(0, os.path.abspath('config'))
  import configure
  configure_options = [
	'PETSC_ARCH=arch-win64',
    '--with-cc=win32fe cl',
    '--with-cxx=win32fe cl',
	'--with-fc=win32fe ifort',
	'--with-ar=win32fe lib',
	'CFLAGS=-EHsc -GA -Oi -Ot -Oy -Ob2 -Gs -GF -Gy -MD',
	'CXXFLAGS=-EHsc -GA -Oi -Ot -Oy -Ob2 -Gs -GF -Gy -MD',
	'FFLAGS=-MD',
	'--doCleanup=1',
	'--with-clanguage=Cxx',
	'--with-c++-support',
	'--ignore-cygwin-link',
	'--with-blas-lib=[/cygdrive/f/Shared/dist/12.0-win64/mkl-11/lib/mkl_blas95_lp64.lib,/cygdrive/f/Shared/dist/12.0-win64/mkl-11/lib/mkl_rt.lib,\
		/cygdrive/f/Shared/dist/12.0-win64/mkl-11/compiler/lib/libiomp5md.lib]',
	'--with-lapack-lib=[/cygdrive/f/Shared/dist/12.0-win64/mkl-11/lib/mkl_lapack95_lp64.lib]',
    '--with-debugging=0',
	'--with-mpi-include=/home/ederson/MSMPI/include',
	'--with-mpi-lib=[/home/ederson/MSMPI/lib/msmpi.lib,/home/ederson/MSMPI/lib/msmpifec.lib]',
	'--with-hypre-include=/home/ederson/external_packages/hypre-2.10.1/include',
	'--with-hypre-lib=/home/ederson/external_packages/hypre-2.10.1/lib/HYPRE.lib',
	'--with-threadsafety',
	'--download-concurrencykit',
	'--with-log=0',
	'--with-shared-libraries',
	'--useThreads=0',	
	'--useThreads=0',
  ]
  configure.petsc_configure(configure_options)
